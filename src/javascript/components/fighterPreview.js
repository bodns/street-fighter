import {createElement} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);

    const fighterData = [
      createTextElement(fighter.name),
      createTextElement("Health: " + fighter.health),
      createTextElement("Attack: " + fighter.attack),
      createTextElement("Defense: " + fighter.defense)
    ].reverse();

    fighterElement.append(fighterImage, ...fighterData);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createTextElement(text) {
  const textElement =  createElement({
    tagName: 'span',
    className: 'fighter-preview___fighter_info'
  });

  textElement.textContent = text;
  return textElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  return createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });
}
