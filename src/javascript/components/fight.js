import { controls } from '../../constants/controls';

export const CRITICAL_HIT_COOLDOWN = 10000; // 10sec

function random(min = 0, max = 1) {
  return min + Math.random() * (max - min);
}

function prepareFighterData(fightData, fighter, id) {
  Object.assign(fighter, {
    _fightID: id,
    _healthBar: document.getElementById(id === 1 ? "left-fighter-indicator" : "right-fighter-indicator")
  });

  fightData.set(id, {
    health: fighter.health,
    lastCriticalHitTime: 0,
    isBlocking: false
  });
}

function updateHealthBar(fighter, fightData) {
  const { health } = fightData.get(fighter._fightID);
  fighter._healthBar.style.width = Math.max(0, Math.floor(100 / fighter.health * health)) + "%";
}

function attack(attacker, victim, fightData) {
  const attackerData = fightData.get(attacker._fightID);
  const victimData = fightData.get(victim._fightID);
  if (attackerData.isBlocking || victimData.isBlocking) return false;
  const damage = getDamage(attacker, victim);
  victimData.health -= damage;

  updateHealthBar(victim, fightData);

  return victimData.health <= 0;
}

function attackCritical(attacker, victim, fightData) {
  const attackerData = fightData.get(attacker._fightID);
  const victimData = fightData.get(victim._fightID);
  if (attackerData.lastCriticalHitTime + CRITICAL_HIT_COOLDOWN > Date.now() || attackerData.isBlocking) return false;
  attackerData.lastCriticalHitTime = Date.now();
  const damage = getCriticalDamage(attacker);
  victimData.health -= damage;

  updateHealthBar(victim, fightData);

  return victimData.health <= 0;
}

function processKey(downedKeys, [firstFighter, secondFighter], fightData, onBattleEnd) {
  const isKeyDown = (key) => downedKeys.has(key);
  const isKeyCombinationDown = (keyCombination) => keyCombination.every(isKeyDown);

  const processNormal = (attacker, victim) => {
    const isAttackerWon = attack(attacker, victim, fightData);
    if (isAttackerWon) onBattleEnd(attacker);
  }
  const processCritical = (attacker, victim) => {
    const isAttackerWon = attackCritical(attacker, victim, fightData);
    if (isAttackerWon) onBattleEnd(attacker);
  }

  const firstFighterData = fightData.get(firstFighter._fightID);
  const secondFighterData = fightData.get(secondFighter._fightID);
  firstFighterData.isBlocking = isKeyDown(controls.PlayerOneBlock);
  secondFighterData.isBlocking = isKeyDown(controls.PlayerTwoBlock);

  if (isKeyCombinationDown(controls.PlayerOneCriticalHitCombination)) return processCritical(firstFighter, secondFighter);
  if (isKeyCombinationDown(controls.PlayerTwoCriticalHitCombination)) return processCritical(secondFighter, firstFighter);
  if (isKeyDown(controls.PlayerOneAttack)) return processNormal(firstFighter, secondFighter);
  if (isKeyDown(controls.PlayerTwoAttack)) return processNormal(secondFighter, firstFighter);
}

export async function fight(firstFighter, secondFighter) {
  const fightData = new Map();
  prepareFighterData(fightData, firstFighter, 1);
  prepareFighterData(fightData, secondFighter, 2);

  const downedKeys = new Set();

  return new Promise((resolve) => {
    const onBattleEnd = (winner) => {
      document.removeEventListener("keydown", onKeyDown);
      document.removeEventListener("keyup", onKeyUp);
      resolve(winner);
    }
    const onKeyDown = (event) => {
      if (downedKeys.has(event.code)) return;
      downedKeys.add(event.code);
      processKey(downedKeys, [firstFighter, secondFighter], fightData, onBattleEnd);
    }
    const onKeyUp = (event) => {
      if (downedKeys.has(event.code)) downedKeys.delete(event.code);
    }
    document.addEventListener("keydown", onKeyDown, false);
    document.addEventListener("keyup", onKeyUp, false);
  });
}

export function getCriticalDamage(attacker) {
  return getHitPower(attacker) * 2;
}

export function getHitPower(fighter) {
  const criticalHitChance = random(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = random(1, 2);
  return fighter.defense * dodgeChance;
}

export function getDamage(attacker, defender) {
  return Math.max(getHitPower(attacker) - getBlockPower(defender), 0);
}