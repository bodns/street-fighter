import {showModal} from "./modal";
import {createFighterImage} from "../fighterPreview";

export function showWinnerModal(fighter) {
  showModal({
      title: `Player ${fighter._fightID} won!`,
      bodyElement: createFighterImage(fighter),
      onClose: () => location.reload()
  });
}
