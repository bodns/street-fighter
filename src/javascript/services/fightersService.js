import {callApi} from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';
  #details_endpoint = `details/fighter/%id%.json`;

  async getFighters() {
    try {
      return await callApi(this.#endpoint);
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      return await callApi(this.#details_endpoint.replace("%id%", id));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
